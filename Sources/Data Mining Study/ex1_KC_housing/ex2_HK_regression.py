# https://www.springboard.com/blog/data-mining-python-tutorial/
"""First we import statsmodels to get the least squares regression estimator function.
The “Ordinary Least Squares” module will be doing the bulk of the work when it comes to crunching numbers for regression in Python.
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import seaborn as sns
from matplotlib import rcParams

import statsmodels.api as sm
from statsmodels.formula.api import ols

"""When you code to produce a linear regression summary with OLS with only two variables this will be the formula that you use:

Reg = ols(‘Dependent variable ~ independent variable(s), dataframe).fit()
print(Reg.summary())
"""
## TODO: Understanding Regression

df = pd.read_csv('kc_house_data.csv')
m = ols('price ~ sqft_living',df).fit()
print (m.summary())

"""This relationship also has a decent magnitude – for every additional 100 square-feet a house has, we can predict that house to be priced $28,000 dollars higher on average. It is easy to adjust this formula to include more than one independent variable, simply follow the formula:

Reg = ols(‘Dependent variable ~ivar1 + ivar2 + ivar3… + ivarN, dataframe).fit()
print(Reg.summary())
"""

m = ols('price ~ sqft_living + bedrooms + grade + condition',df).fit()
print (m.summary())
# An example of multivariate linear regression.

# Visualizing the regression results.
sns.jointplot(x="sqft_living", y="price", data=df, kind = 'reg',fit_reg= True, size = 7)
plt.show()
