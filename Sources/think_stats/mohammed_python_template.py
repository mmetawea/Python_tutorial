#!/usr/bin/python
from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from httplib2 import Http
from decimal import Decimal
from collections import defaultdict
import datetime

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

import MySQLdb

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = '' #ENTER APPLICATION NAME HERE
SHEET_ID = '' #ENTER SHEET ID HERE

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

def execute_db_query(query):
    db = MySQLdb.connect(host="rds-xo-prod.crossover.com",
                         user="pxo001_readonly",
                         passwd="b9xu8GR4",
                         db="pxo001_xo_prod",
                         port=3306)
    cur = db.cursor()
    cur.execute(query)
    results = cur.fetchall()
    db.close()
    return results

def get_cur():
    db = MySQLdb.connect(host="rds-xo-prod.crossover.com",
                         user="pxo001_readonly",
                         passwd="b9xu8GR4",
                         db="pxo001_xo_prod",
                         port=3306)
    cur = db.cursor()
    return (cur, db)

def post_data_to_sheet(spreadsheet, fields, sheet_range, rows_list, sheet_id):
    data = {'values': rows_list }
    data['values'].insert(0, (fields))
    empty_body = {}
    spreadsheet.spreadsheets().values().clear(spreadsheetId=sheet_id, range=sheet_range, body=empty_body).execute()
    spreadsheet.spreadsheets().values().update(spreadsheetId=sheet_id, range=sheet_range, body=data, valueInputOption='RAW').execute()

def read_data_from_sheet(spreadsheet, sheet_range, sheet_id):
    results = spreadsheet.spreadsheets().values().get(spreadsheetId=sheet_id, range=sheet_range).execute()
    results = results.get('values', [])
    fields = results[0]
    rows = results[1:]
    return (fields, rows)

def get_query_map(query):
    query_map =  defaultdict(lambda : "NOT FOUND")
    results = execute_db_query(query)
    for result in results:
        req_id = str(result[1])
        value = result[0]
        query_map[req_id] = value
    return query_map

def get_hiring_manager_ids_map():
    query = "select concat(first_name, \" \", last_name), user_id from managers, avatars, users where managers.id = avatars.id and avatars.user_id = users.id;"
    return get_query_map(query)

def get_req_ids_map():
    query = "select title, id from jobs;"
    return get_query_map(query)

def get_hiring_manager_companies_map():
    query = "select companies.name, users.id from managers, companies, avatars, users where managers.company_id = companies.id and managers.id = avatars.id and avatars.user_id = users.id;"
    return get_query_map(query)

def get_pipeline_status_map():
    query = "select status, jobs.id from jobs;"
    return get_query_map(query)

def get_company_type_map():
    query = "select xo_percentage, name from companies;"
    return get_query_map(query)

def main():
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    SHEETS = discovery.build('sheets', 'v4', http=credentials.authorize(Http()))



if __name__ == '__main__':
    main()
