## Feature extraction from text
# Method: bag of words
# https://pythonprogramminglanguage.com
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import euclidean_distances

filename = r'C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input\\input_small.csv'
df = pd.read_csv(filename, usecols=['Message'])


def NormalizFn(input_str):
    """clean special chars and extra whitespace"""
    # input_str.Message = re.sub("\W", "", input_str.Message).strip()
    # sub(​pattern, repl, string​)
    # print re.sub(r"[ab]", "z", "abcABC", flags=re.IGNORECASE)
    # input_str.apply(lambda x: normalize('NFC', x['Message'].loc[1:]))
    input_str.Message = input_str.Message.str.lower()
    input_str.Message = input_str.Message.str.strip()
    input_str.Message = input_str.Message.str.replace("[0-9]{4,}", 'XX')
    # Striping (non-relevant) punctuation
    # input_str.Message.translate(str.maketrans("", "", ",.-'\"():;+/?$°@"))
    # Stripping accents
    # input_str = ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')
    return input_str

df = NormalizFn(df)
corpus = df.Message


vectorizer = CountVectorizer()
features = vectorizer.fit_transform(corpus).todense()
print( vectorizer.vocabulary_ )

# for f in features:
#     print( euclidean_distances(features[0], f) )

print(euclidean_distances(features))
