import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

df = pd.DataFrame(
    data=np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]]), index= [2.5, 12.6, 4.8], columns=[48, 49, 50]
)

#d = {'col1': [1, 2], 'col2': [3, 4]}
# Use the `shape` property
print(df,"\n")

#print(df.loc[0])
# Or use the `len()` function with the `index` property
#print(len(df.index))
#print(df.shape)

https://stackoverflow.com/questions/19084443/replacing-digits-with-str-replace
import re
s = "_u1_v1"
print re.sub('\d', '%d', s)
_u%d_v%d


# Display function defintion
class display(object):
    #Display HTML representation of multiple objects
    template = <div style="float: left; padding: 10px;">
    <p style='font-family:"Courier New", Courier, monospace'>{0}</p>{1}
    </div>
    def __init__(self, *args):
        self.args = args

    def _repr_html_(self):
        return '\n'.join(self.template.format(a, eval(a)._repr_html_())
                         for a in self.args)

    def __repr__(self):
        return '\n\n'.join(a + '\n' + repr(eval(a))
                           for a in self.args)
#print(display(msgdata))


>>> def fn(x): return x*x
...
>>> df = df.assign(count = fn(df.index))
#To remove the grouping variable
# withoutgrp <- desired %>% select(-grp)
>>> print(df)
    A         B  count
0  1   1.465649  0
1  2  -0.225776  1
2  3   0.067528  4
3  4  -1.424748  9
4  5  -0.544383  16
5  6   0.110923  25
6  7  -1.150994  36
7  8   0.375698  49
8  9  -0.600639  64
9  10 -0.291694  81

[10 rows x 3 columns]


Fuzzy.


extractions = fuzzyprocess.extractBests(
            expectation, [str(a) for a in self.addresses],
            limit=limit)
        result = []
        for extraction in extractions:
            result.extend([(x, extraction[1]) for x in self.addresses
                           if str(x) == extraction[0]])
        return result


def get_best_fuzzy(name, choices, cutoff=0):
    items = process.extractBests(name, choices, score_cutoff=cutoff)
    if items:
        scores = [s[1] for s in items]
        morethanone = sum(np.max(scores) == scores) > 1
        if morethanone:
            exact = [s for s in items if s[0].name.lower() == name.lower()]
            if exact:
                return exact[0]
            else:
                options = [s[0].name for s in items if s[1] == np.max(scores)]
                raise KeyError('{0} is too ambiguous.  Did you mean one of {1}?'.format(name, options))
        else:
            return items[0]
    else:
        return None


https://stackoverflow.com/questions/16476924/how-to-iterate-over-rows-in-a-dataframe-in-pandas
pd.DataFrame.itertuples

https://stackoverflow.com/questions/48314657/how-to-store-values-from-loop-to-a-dataframe
store data from a loop
