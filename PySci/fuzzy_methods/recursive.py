#!/usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
import Levenshtein
from Levenshtein import jaro

college_list = ['Dave', 'Jack', 'Josh', 'Donald', 'Carry', 'Kerry', 'Cole', 'Coal', 'Coala']
for pair in itertools.combinations(college_list, 2):
    similarity = Levenshtein.jaro(pair[0], pair[1])
    if similarity >= 0.90 and similarity != 1.0:
        print(pair, similarity)


college_list.sort(key=len)
for i, coll1 in enumerate(college_list):
    for j in range(i + 1, len(college_list)):
        coll2 = college_list[j]
        if len(coll2) - len(coll1) > 15:
            break
        similarity = jaro(coll1,coll2)
        if similarity >= 0.90 and similarity != 1.0:
            print(coll2, coll1, "Probable Similar Strings")
