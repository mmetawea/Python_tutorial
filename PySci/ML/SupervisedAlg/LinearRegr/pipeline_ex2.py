import pandas as pd
import numpy as np
from scipy import sparse

from nltk.stem import WordNetLemmatizer

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline, make_pipeline, FeatureUnion
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score, GridSearchCV, RandomizedSearchCV
from sklearn.metrics import roc_auc_score

# Load and split the data
iris = load_iris()
X_train, X_test, y_train, y_test = train_test_split(iris.data, iris.target, test_size=0.2, random_state=42)

# default params
scoring='roc_auc'
cv=3
n_jobs=-1
max_features = 2500

class NBFeaturer(BaseEstimator):
    def __init__(self, alpha):
        self.alpha = alpha

    def preprocess_x(self, x, r):
        return x.multiply(r)

    def pr(self, x, y_i, y):
        p = x[y==y_i].sum(0)
        return (p+self.alpha) / ((y==y_i).sum()+self.alpha)

    def fit(self, x, y=None):
        self._r = sparse.csr_matrix(np.log(self.pr(x,1,y) / self.pr(x,0,y)))
        return self

    def transform(self, x):
        x_nb = self.preprocess_x(x, self._r)
        return x_nb

# Simple pipelines of default sklearn TfidfVectorizer to prepare features and Logistic Reegression to make predictions
tfidf = TfidfVectorizer(max_features=max_features)

class Lemmatizer(BaseEstimator):
    def __init__(self):
        self.l = WordNetLemmatizer()

    def fit(self, x, y=None):
        return self

    def transform(self, x):
        x = map(lambda r:  ' '.join([self.l.lemmatize(i.lower()) for i in r.split()]), x)
        x = np.array(list(x))
        return x

lm = Lemmatizer()
tfidf_w = TfidfVectorizer(max_features=max_features, analyzer='word')
tfidf_c = TfidfVectorizer(max_features=max_features, analyzer='char')
lr = LogisticRegression()
nb = NBFeaturer(1)
p = Pipeline([
    ('lm', lm),
    ('wc_tfidfs',
         FeatureUnion([
            ('tfidf_w', tfidf_w),
            ('tfidf_c', tfidf_c),
         ])
    ),
    ('nb', nb),
    ('lr', lr)
])



cross_val_score(estimator=p, X=X_train, y=y_train, scoring=scoring, cv=cv, n_jobs=n_jobs)
