# In the previous example, we validated the performance of our model using the learning data. However, is this now a good option, given that our algorithm can either underfit of overfit the data? Let’s take a look at the simpler example when we have one feature which represents the size of a house and another which represents its price.

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_score

import matplotlib.pyplot as plt
import numpy as np

# Ground truth function
ground_truth = lambda X: np.cos(15 + np.pi * X)

# Generate random observations around the ground truth function
n_samples = 15
degrees = [1, 4, 30]

X = np.linspace(-1, 1, n_samples)
y = ground_truth(X) + np.random.randn(n_samples) * 0.1

plt.figure(figsize=(14, 5))
# plt.plot
# plt.figure()

models = {}

# Plot all machine learning algorithm models
for idx, degree in enumerate(degrees):
    ax = plt.subplot(1, len(degrees), idx + 1)
    plt.setp(ax, xticks=(), yticks=())

    # Define the model
    polynomial_features = PolynomialFeatures(degree=degree )
    model = make_pipeline(polynomial_features, LinearRegression())

    models[degree] = model

    # Train the model
    model.fit(X[:, np.newaxis], y)

    # Evaluate the model using cross-validation
    scores = cross_val_score(model, X[:, np.newaxis], y, cv=3)

    X_test = X
    plt.plot(X_test, model.predict(X_test[:, np.newaxis]), label="Model")
    plt.scatter(X, y, edgecolor='b', s=20, label="Observations")

    plt.xlabel("x")
    plt.ylabel("y")
    plt.ylim((-2, 2))

    plt.title("Degree {}\nMSE = {:.2e}".format(
        degree, -scores.mean()))

plt.show()

# The machine learning algorithm model is underfitting if it can generalize neither the training data nor new observations. In the example above, we use a simple linear hypothesis which does not really represent the actual training dataset and will have very poor performance. Usually, underfitting is not discussed as it can be easily detected given a good metric.
#
# If our algorithm remembers every single observation it was shown, then it will have poor performance on new observations outside of the training dataset. This is called overfitting. For example, a 30th-degree polynomial model passes through the most of the points and has a very good score on the training set, but anything outside of that would perform badly.
#
# Our dataset consists of one feature and is simple to plot in 2D space; however, in real life, we may have datasets with hundreds of features, which makes them impossible to plot visually in Euclidean space. What other options do we have in order to see if the model is underfitting or overfitting?
#
# It’s time to introduce you to the concept of the learning curve. This is a simple graph that plots the mean squared error over the number of training samples.
#

from sklearn.model_selection import learning_curve, ShuffleSplit

# Plot learning curves
plt.figure(figsize=(20, 5))

for idx, degree in enumerate(models):
    ax = plt.subplot(1, len(degrees), idx + 1)

    plt.title("Degree {}".format(degree))
    plt.grid()

    plt.xlabel("Training examples")
    plt.ylabel("Score")

    train_sizes = np.linspace(.6, 1.0, 6)

    # Cross-validation with 100 iterations to get a smoother mean test and training
    # score curves, each time with 20% of the data randomly selected as a validation set.
    cv = ShuffleSplit(n_splits=100, test_size=0.2, random_state=0)

    model = models[degree]
    train_sizes, train_scores, test_scores = learning_curve(
        model, X[:, np.newaxis], y, cv=cv, train_sizes=train_sizes, n_jobs=4)

    train_scores_mean = np.mean(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)

    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training score")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Test score")

    plt.legend(loc = "best")

plt.show()

#
# In our simulated scenario, the Red line, which represents the training score, seems like a straight line. In reality, it still slightly decreases—you can actually see this in the first-degree polynomial graph, but in the others it’s too subtle to tell at this resolution. We at least clearly see that there is a huge gap between learning curves for training and test observations with a “high bias” scenario.
#
# On the “normal” learning rate graph in the middle, you can see how training score and test score lines come together.
#
# And on the “high variance” graph, you can see that with a low number of samples, the test and training scores are very similar; however, when you increase the number of samples, the training score remains almost perfect while the test score grows away from it.
#
# We can fix underfitting models (also called models with high bias) if we use a non-linear hypothesis, e.g., the hypothesis with more polynomial features.
#
# Our overfitting model (high variance) passes through every single example it is shown; however, when we introduce test data, the gap between learning curves widens. We can use regularization, cross-validation, and more data samples to fix overfitting models.
