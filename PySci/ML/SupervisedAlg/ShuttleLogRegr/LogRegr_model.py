# https://www.toptal.com/machine-learning/supervised-machine-learning-algorithms

# Sigmoid function  to convert any numerical value to represent a value on the interval [ −1 , 1 ]
# f(x) = 1 / (1 + (e^x))
# Now, instead of x, we need to pass an existing hypothesis and therefore we will get:
# f(x) = 1 / (1 + (e^ (θ-0 + θ1 X1 + .. + θn Xn) ))

# After that, we can apply a simple threshold saying that if the hypothesis is greater than zero, this is a true value, otherwise false.

# hθ(x) = { 1  if θT X > 0
#         { 0 else

# Our dataset is complete, meaning that there are no missing features; however, some of the features have a “*” instead of the category, which means that this feature does not matter. We will replace all such asterisks with zeroes.

from sklearn.linear_model import LogisticRegression
import os
from pandas import read_csv

# Data
data_path = os.path.join(os.getcwd(), "data/shuttle-landing-control.data")
dataset = read_csv(data_path, header=None,
                    names=['Auto', 'Stability', 'Error', 'Sign', 'Wind', 'Magnitude', 'Visibility'],
                    na_values='*').fillna(0)# Prepare features
X = dataset[['Stability', 'Error', 'Sign', 'Wind', 'Magnitude', 'Visibility']]
y = dataset[['Auto']].values.reshape(1, -1)[0]

model = LogisticRegression(solver='lbfgs')
model.fit(X, y)

model.score(X, y)

# For now, we're missing one important concept. We don't know how well our model
# works, and because of that, we cannot really improve the performance of our hypothesis.
# There are a lot of useful metrics, but for now, we will validate how well
# our algorithm performs on the dataset it learned from.
"Score of our model is %2.2f%%" % (model.score(X, y) * 100)

# Prepare features
X = dataset[['Stability', 'Error', 'Sign', 'Wind', 'Magnitude', 'Visibility']]
y = dataset[['Auto']].values.reshape(1, -1)[0]

model = LogisticRegression(solver='lbfgs')
model.fit(X, y)

# For now, we're missing one important concept. We don't know how well our model
# works, and because of that, we cannot really improve the performance of our hypothesis.
# There are a lot of useful metrics, but for now, we will validate how well
# our algorithm performs on the dataset it learned from.
# "Score of our model is %2.2f%%" % (model.score(X, y) * 100)
# Print(model.score(X, y )*100)
z =model.score(X, y )*100
print(z)
