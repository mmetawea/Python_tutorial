import nltk
import re
import unicodedata
import urllib.request
from bs4 import BeautifulSoup
from nltk.corpus import stopwords


def webCrawl(website):
    # ss = requests.Session()
    response = urllib.request.urlopen('http://php.net/')
    html = response.read()
    soup = BeautifulSoup(html,"html5lib")
    text = soup.get_text(strip=True, separator=" ")
    return text
    # print (str(text))  # where you can see if the html is parsed normally or not

def normFn(input_str):
    """clean special chars and extra whitespace"""
    # sub(​pattern, repl, string​)
    # input_str = re.sub("\W", "", input_str).strip()
    input_str = input_str.lower()
    input_str = input_str.strip()
    input_str = input_str.replace("[0-9]{4,}", 'XX')
    # Striping (non-relevant) punctuation
    input_str.translate(str.maketrans("", "", ",.-'\"():;+/?$°@"))
    # Stripping accents
    input_str = ''.join(c for c in unicodedata.normalize('NFC', input_str))
    # s1 = normalize('NFC', u1)  # get u1 NFC format
    return input_str



website = 'http://php.net/'

text = webCrawl(website)
text = normFn(text)

### Tokenization and removing Stop Words
tokens = [t for t in text.split()]
clean_tokens = tokens[:]
sr = stopwords.words('english')

for token in tokens:
    if token in stopwords.words('english'):
        clean_tokens.remove(token)
freq = nltk.FreqDist(clean_tokens)

for key,val in freq.items():
    print (str(key) + ':' + str(val))



freq.plot(20, cumulative=False)
# show.plot()
