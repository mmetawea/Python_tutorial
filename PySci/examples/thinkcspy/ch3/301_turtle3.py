import turtle
#Tutle clock

wn = turtle.Screen() # Set up the window and its attributes

wn.bgcolor("lightgreen")
wn.title("Tess")

tess = turtle.Turtle() # Create tess and set some attributes
tess.shape("turtle")
#tess.penup()
tess.speed(100)
tess.color("black")
step=30
# Function Definition
#  def NAME( PARAMETERS ):
#  STATEMENTS
#

def clock(self):
    for i in range(12):
        tess.hideturtle()
        tess.penup()
        tess.forward(50)
        tess.pendown()
        tess.forward(15)
        tess.penup()
        tess.forward(10)
        tess.stamp()
        tess.home()
        tess.left(step)
        step=step+30

tess.home()
tess.showturtle()

if __name__ == '__main__':
    print("This only executes when %s is executed rather than imported" % __file__)

wn.exitonclick()
