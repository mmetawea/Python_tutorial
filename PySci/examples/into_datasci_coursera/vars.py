x = 'This is a string'
print(x[0]) #first character
print(x[0:1]) #first character, but we have explicitly set the end character
print(x[0:2]) #first two characters


firstname = 'Christopher'
lastname = 'Brooks'

print(firstname + ' ' + lastname)
print(firstname*3)
# print(r'[Cc]hris' in firstname)

x = ('Christopher', 'Brooks', 'brooksch@umich.edu')
fname, lname, email = x
print(fname, lname, email)

print('Chris' + 2)
print('Chris' + str(2))

sales_record = {'price': 3.24, 'num_items': 4, 'person': 'Chris'}

sales_statement = '{} bought {} item(s) at a price of {} each for a total of {}'

print(sales_statement.format(sales_record['person'],
                             sales_record['num_items'],
                             sales_record['price'],
                             sales_record['num_items']*sales_record['price']))

x = ('Christopher Brooks', 'brooksch@umich.edu')

for name, email in x.items():
    print(name)
    print(email)


l = [1, 7, 3, 5]
for i in xrange(len(l) - 1):
    x = l[i]
    y = l[i + 1]

def grouper(input_list, n = 2):
    for i in xrange(len(input_list) - (n - 1)):
        print(type(i))
        yield input_list[i:i+n]

for first, second, third in grouper([1, 7, 3, 5, 6, 8], 3):
    print (first, second, third)

x = ('Christopher Brooks', 'brooksch@umich.edu')
type(x)
zipped = zip(x)
list(zipped)
