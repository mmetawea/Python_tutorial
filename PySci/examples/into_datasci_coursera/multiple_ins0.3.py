### URL: https://docs.python.org/2/tutorial/controlflow.html#unpacking-argument-lists

range(3, 6)
args = [3, 6]
range(*args)
list(range(*args))


def parrot(voltage, state='a stiff', action='voom'):
     print( "-- This parrot wouldn't", action)
     print( "if you put", voltage, "volts through it.")
     print( "E's", state, "!")

d = {"voltage": "four million", "state": "bleedin' demised", "action": "VOOM"}

parrot(**d)

def make_incrementor(n):
     return lambda x: x + n

f = make_incrementor(42)
f(0)
f(1)

## Sorting whether numerically or alphabatically
pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
pairs.sort(key=lambda pair: pair[0])

pairs[1]

# pairs.sort(key=pairs[1][0:])
pairs[1][0]
pairs

# >>> pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
# >>> pairs.sort(key=lambda pair: pair[1])
# >>> pairs
# [(4, 'four'), (1, 'one'), (3, 'three'), (2, 'two')]
