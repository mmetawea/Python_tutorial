import re
import pandas as pd
import numpy as np
from numpy import genfromtxt
from collections import Counter


def Readfn():
    """load the CSV file"""
    datafile = pd.read_csv(r"C:\Users\Mohammed\Google Drive\workplace\Insights\input\input.csv", usecols=['Time','Source','Message'], parse_dates=['Time'],
                           index_col=None, dtype={"Source": str, "Message": str}, infer_datetime_format=True, engine='c' )#.groupby('Message')
    datafile.Message=datafile.Message.str.replace("[0-9]{3,}", 'XX')
    datafile.drop(columns=(["Time", "Source"]), inplace=True)
    return datafile

# def npReadfn(filename):
#     my_data = genfromtxt(filename)
# import csv
# import numpy as np
# with open(dest_file,'r') as dest_f:
#     data_iter = csv.reader(dest_f,
#                            delimiter = delimiter,
#                            quotechar = '"')
#     data = [data for data in data_iter]
# data_array = np.asarray(data, dtype = <whatever options>)

df=Readfn()
data_items = ['abc','123data','dataxyz','456','344','666','777','888','888', 'abc', 'xyz']
#data_items = pd.Series.tolist(df)
# data_items = df
# print(data_items)
print(type(data_items))
search = ['Google','Number','YouTube','CEX', 'ata']

to_search = re.compile('|'.join(sorted(search, key=len, reverse=True)))
matches = (to_search.search(el) for el in data_items)
counts = Counter(match.group() for match in matches if match)

print(matches)
print(counts)
# Counter({'abc': 2, 'xyz': 2, '123': 1, '456': 1})
#df = pd.DataFrame.from_dict(counts, orient = "index").reset_index()

# df2=npReadfn('.\input\input.csv')

### Turn dict into DataFrame
# from collections import Counter
# d = Counter({'fb_view_listing': 76, 'fb_homescreen': 63, 'rt_view_listing': 50, 'rt_home_start_app': 46, 'fb_view_wishlist': 39, 'fb_view_product': 37, 'fb_search': 29, 'rt_view_product': 23, 'fb_view_cart': 22, 'rt_search': 12, 'rt_view_cart': 12, 'add_to_cart': 2, 'create_campaign': 1, 'fb_connect': 1, 'sale': 1, 'guest_sale': 1, 'remove_from_cart': 1, 'rt_transaction_confirmation': 1, 'login': 1})
# df = pd.DataFrame.from_dict(d, orient='index').reset_index()
#myseries.to_frame(name='list').reset_index()

# letters_only = re.sub("[^a-zA-Z]",  # Search for all non-letters
#                           " ",          # Replace all non-letters with spaces
#                           str(location))

"""
[value for key, value in programs.items() if 'new york' in key.lower()]



As I can see, you're asking that for each String in your mass variable, is there any substring, which represents a key in Map

If that's the scenario, then you'd want to get multiple loops, one to iterate over your String and another to iterate over the your Map.

for (String s : mass ) {
    for (Map.Entry<String, String> entry : map.entrySet())  {
        if (s.contains(entry.getKey())) {
            form.setField(entry.getKey(), entry.getValue());
            break;
        }
    }
}



for (String s : mass ) {
    Optional<String> key = map.keySet().parallelStream().filter(s::contains).findFirst();
    if(key.isPresent()) {
        form.setField(key.get(), map.get(key.get()));
    }
}
"""
