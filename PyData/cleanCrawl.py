from pyparsing import makeHTMLTags, SkipTo, anyOpenTag, anyCloseTag, ParseException

htmlsrc = """<A HREF="notizia.php?idn=1134" OnMouseOver="verde();" OnMouseOut="blu();"><FONT CLASS="v12"><B>03-11-2009:&nbsp;&nbsp;<font color=green>CCS Ingegneria Elettronica-Sportello studenti ed orientamento</B></FONT></A>"""

# create pattern to find interesting <A> tags
aStart,aEnd = makeHTMLTags("A")
def matchInterestingHrefsOnly(t):
    if not t.href.startswith("notizia.php?"):
        raise ParseException("not interested...")
aStart.setParseAction(matchInterestingHrefsOnly)
patt = aStart + SkipTo(aEnd)("body") + aEnd

# create pattern to strip HTML tags, and convert HTML entities
stripper = anyOpenTag.suppress() | anyCloseTag.suppress()
def stripTags(s):
    s = stripper.transformString(s)
    s = s.replace("&nbsp;"," ")
    return s


for match in patt.searchString(htmlsrc):
    print( stripTags(match.body))
