#########   Building Your Model
# You will use the scikit-learn library to create your models. When coding, this library is written as sklearn, as you will see in the sample code. Scikit-learn is easily the most popular library for modeling the types of data typically stored in DataFrames.
#
# The steps to building and using a model are:
#
# Define: What type of model will it be? A decision tree? Some other type of model? Some other parameters of the model type are specified too.
# Fit: Capture patterns from provided data. This is the heart of modeling.
# Predict: Just what it sounds like
# Evaluate: Determine how accurate the model's predictions are.
# Here is an example of defining a decision tree model with scikit-learn and fitting it with the features and target variable.


# Code you have previously used to load data
import pandas as pd

# Path of the file to read
iowa_file_path = '../input/home-data-for-ml-course/train.csv'
home_data = pd.read_csv(iowa_file_path)


## Step 1: Specify Prediction Target
# Select the target variable, which corresponds to the sales price. Save this to a new variable called `y`. You'll need to print a list of the columns to find the name of the column you need.

print (home_data.head())
# print the list of columns in the dataset to find the name of the prediction target
print(home_data.columns)

## Choosing y
y = home_data.SalePrice

## Step 2: Create X
# Now you will create a DataFrame called `X` holding the predictive features.
# Since you want only some columns from the original data, you'll first create a list with the names of the columns you want in `X`.

# Create the list of features below
feature_names = [ 'LotArea', 'YearBuilt', '1stFlrSF', '2ndFlrSF', 'FullBath', 'BedroomAbvGr', 'TotRmsAbvGrd']

# select data corresponding to features in feature_names
X = home_data[feature_names]


# Review data
# print description or statistics from X
print(X.describe())

# print the top few lines
print(X.head())

## Step 3: Specify and Fit Model
# Create a `DecisionTreeRegressor` and save it iowa_model. Ensure you've done the relevant import from sklearn to run this command.
# Then fit the model you just created using the data in `X` and `y` that you saved above.

from sklearn.tree import DecisionTreeRegressor
#specify the model.
# For model reproducibility, set a numeric value for random_state when specifying the model
iowa_model = DecisionTreeRegressor(random_state=10)

# Fit the model
iowa_model.fit(X, y)

## Step 4: Make Predictions
# Make predictions with the model's `predict` command using `X` as the data. Save the results to a variable called `predictions`.


predictions = iowa_model.predict(X)
print(predictions)


#### Model Validation

from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error

# split data into training and validation data, for both features and target
# The split is based on a random number generator. Supplying a numeric value to
# the random_state argument guarantees we get the same split every time we
# run this script.
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state = 0)
# Define model
iowa_model = DecisionTreeRegressor()
# Fit model
iowa_model.fit(train_X, train_y)

# get predicted prices on validation data
val_predictions = melbourne_model.predict(val_X)
print(mean_absolute_error(val_y, val_predictions))
