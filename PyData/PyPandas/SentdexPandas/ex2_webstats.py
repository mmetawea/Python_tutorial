import pandas as pd
import numpy as np
import datetime
##import pandas.io.data as web
#import pandas_datareader
#from pandas_datareader import data, wb
import matplotlib.pyplot as plt
from matplotlib import style
style.use('ggplot')

web_stats = {'Day':[1,2,3,4,5,6],
             'Visitors':[43,34,65,56,29,76],
             'Bounce Rate':[65,67,78,65,45,52]}

df = pd.DataFrame(web_stats)
print(df)
print(df['Day'][2])
#Column Dat, Row = 2

df2 = df.set_index ('Day')
df.set_index('Day', inplace=True)
df['Day']['Bounce Rate'].plot()
print(df['Visitors'])
print(df["Bounce Rate"])

#Multiple Columns
print(df[['Visitors', "Bounce Rate"]])

#Return a list from a Column
print(df.Visitors.tolist())

# Return a list of multiple columns
print(np.array(df[['Visitors', "Bounce Rate"]]))

# Return an array of multiple DF Columns
df3 = pd.DataFrame(np.array(df[['Visitors', "Bounce Rate"]]))
print(df3)
