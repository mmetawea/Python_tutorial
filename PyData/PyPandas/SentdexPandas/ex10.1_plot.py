import numpy as np
import scipy.stats
# import matplotlib.mlab as mlab
# import matplotlib.backends.backend_tkagg
# matplotlib.use("Qt5Agg")
import matplotlib
import matplotlib.pyplot as plt
# from matplotlib import interactive
# interactive(True)

mu, sigma = 100, 15
x = mu + sigma*np.random.randn(10000)

# the histogram of the data
n, bins, patches = plt.hist(x, 50, density=1, facecolor='green', alpha=0.75)

# add a 'best fit' line
y = scipy.stats.norm.pdf( bins, mu, sigma)
l = plt.plot(bins, y, 'r--', linewidth=1)

plt.xlabel('Smarts')
plt.ylabel('Probability')
plt.title(r'$\mathrm{Histogram\ of\ IQ:}\ \mu=100,\ \sigma=15$')
plt.axis([40, 160, 0, 0.03])
plt.grid(True)

plt.show()
# from matplotlib.backends.qt_compat import QtGui, QtWidgets, QtCore
# QtGui, QtWidgets, QtCore
