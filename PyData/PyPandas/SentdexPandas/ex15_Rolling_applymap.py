import os
import quandl
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import style
from statistics import mean

style.use('fivethirtyeight')
os.chdir('C:\\Python\\gitlab\\Python_tutorial\\examples\\sentdex\\')


# Not necessary, I just do this so I do not show my API key.
api_key = open('C:\\Users\\Mohammed\\gitlab\\quandlapikey.txt','r').read()



def create_labels(cur_hpi, fut_hpi):
    if fut_hpi > cur_hpi:
        return 1
    else:
        return 0


def moving_avg(values):
    return mean(values)


# grab_initial_state_data()
housing_data = pd.read_pickle('HPI.pickle')
housing_data = housing_data.pct_change()

housing_data.replace([np.inf, -np.inf], np.nan, inplace=True)

housing_data['US_HPI_future'] = housing_data['US_HPI'].shift(-1)

housing_data['label'] = list(
    map(create_labels, housing_data['US_HPI'], housing_data['US_HPI_future']))
# housing_data['label'] = housing_data.apply(lambda row: 1 if row['US_HPI_future'] > row['United States'] else 0, axis=1)﻿

housing_data['ma_apply_ex'] = housing_data['M30'].rolling(
    window=10, center=False).apply(moving_avg, raw=False)
housing_data.dropna(inplace=True)

print(housing_data.tail())
