# Python Data Science Handbook - Page 194

import datetime as dt
dt.datetime( year = 2015, month=7, day=4)

import dateutil
date = dateutil.parser.parse('4th of July, 2015')

date.strftime('%A')

import numpy as np
date = np.array('2015-07-10', dtype=np.datetime64)
date
# date.strftime('%A')

date + np.arange(23)

np.datetime64('2015-07-04 12:00')
np.datetime64('2015-07-04 12:59:59.50', 'ns')
np.datetime64('2015-07-04 12:59:59.50', 'ms')
np.datetime64('2015-07-04 12:59:59.50', 'fs')

np.datetime64('2015-07-04 12:59:59.50', 'as')


import pandas as pd
date = pd.to_datetime("4th of July, 2015")

date
date.strftime('%A')

date + pd.to_timedelta(np.arange(12), 'D')
dates = pd.to_datetime( [dt.datetime(2015, 7, 3), '4th of July, 2015','2015-Jul-6', '07-07-2015', '20150708'])
dates

dates.to_period('D')

pd.date_range('2015-07-03', '2015-07-10')
pd.date_range('2015-07-03', periods=8)
pd.period_range('2016-07-03', periods = 12, freq='H')
pd.date_range(0, periods = 12, freq='H')


## Resampling, Shifting & Windowing
