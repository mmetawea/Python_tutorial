import pandas as pd
from datetime import datetime
import numpy as np

date_rng = pd.date_range(start='1/1/2000', end='08/1/2000', freq='H')
type(date_rng[0])

df = pd.DataFrame(date_rng, columns=['date'])
df['data'] = np.random.randint(0,100,size=(len(date_rng)))
df.head(15)

# If we want to do time series manipulation, we’ll need to have a date time index so that our data frame is indexed on the timestamp.
# Convert the data frame index to a datetime index

df['datetime'] = pd.to_datetime(df['date'])
df = df.set_index('datetime')
df.drop(['date'], axis=1, inplace=True)
df

"""What if our ‘time’ stamps in our data are actually string type vs. numerical? Let’s convert our date_rng to a list of strings and then convert the strings to timestamps."""
string_date_rng = [str(x) for x in date_rng]
string_date_rng
timestamp_date_rng = pd.to_datetime(string_date_rng, infer_datetime_format=True)
timestamp_date_rng == df.index


"""But what about if we need to convert a unique string format?
Let’s create an arbitrary list of dates that are strings and convert them to timestamps:"""
string_date_rng_2 = ['June-01-2018', 'June-02-2018', 'June-03-2018']
timestamp_date_rng_2 = [datetime.strptime(x,'%B-%d-%Y') for x in string_date_rng_2]
df2 = pd.DataFrame(timestamp_date_rng_2, columns=['date'])

df[df.index.day ==2]
df['2000-01-04':'2000-01-06']

df
df.resample('24h')
df.resample('24h').mean()
df['data'].resample('24h').mean()
df.data.resample('H').mean()

"""What about window statistics such as a rolling mean or a rolling sum"""
# Let’s create a new column in our original df that computes the rolling sum over a 3 window period and then look at the top of the data frame:
df['rolling_sum'] = df.rolling(3).sum()
#testing rolling mean..
df['rolling_mean'] = df['data'].rolling(3).mean()
df['data'].mean()
DataFrame.mean(axis=None, skipna=None, level=None, numeric_only=None, **kwargs)


"""This is a good chance to see how we can do forward or backfilling of data when working with missing data values.
Here’s our df but with a new column that takes the rolling sum and backfills the data:"""

df['rolling_sum_backfilled'] = df['rolling_sum'].fillna(method='backfill')
df['rolling_mean_backfilled'] = df['rolling_mean'].fillna(method='backfill')
df.head(10)


"""Here’s an example of a time t that is in Epoch time and converting unix/epoch time to a regular time stamp in UTC:"""
epoch_t = 1529272655
real_t = pd.to_datetime(epoch_t, unit='s')
real_t

real_t.tz_localize('UTC').tz_convert('US/Pacific')
