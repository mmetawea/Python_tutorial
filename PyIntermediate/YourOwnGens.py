#Standard Generator
# [i for i in range(5)]
# (i for i in range(5))

# A Custom Generator
# def simple_gen():
#     yield 'Oh'
#     yield 'Hello'
#     yield 'there'
#
# for i in simple_gen():
#     print(i)

# a complete loop
# CORRECT_COMBO = (3, 6, 1)
# for c1 in range(10):
#     for c2 in range(10):
#         for c3 in range(10):
#             if (c1, c2, c3) == CORRECT_COMBO:
#                 print(f'Found the combo: {c1,c2,c3}')
#                 break
#             print(c1,c2,c3)

# A loop that breaks once it founds the key
# CORRECT_COMBO = (3, 6, 1)
# found_combo = False
# for c1 in range(10):
#     for c2 in range(10):
#         if found_combo: break
#         for c3 in range(10):
#             if found_combo: break
#             if (c1, c2, c3) == CORRECT_COMBO:
#                 print(f'Found the combo: {c1,c2,c3}')
#                 found_combo = True
#                 break
#             print(c1,c2,c3)

# A gen function
CORRECT_COMBO = (3, 6, 1)
def combo_gen():
    for c1 in range(10):
        for c2 in range(10):
            for c3 in range(10):
                yield (c1,c2,c3)

for (c1,c2,c3) in combo_gen():
    print(c1, c2, c3)
    if (c1, c2, c3) == CORRECT_COMBO:
        print(f'Found the combo: {c1, c2, c3}')
        break

[(c1,c2,c3) for c1 in range(10) c2 in range(10) c3 in range(10) ]
[(c1,c2,c3) for c1,c2,c3 in range(10)]


inputs = ["1, foo, bar", "2,tom,  jerry"]

outputs1 = [[int(x), y.strip(), z.strip()] for x,y,z in (s.split(',') for s in inputs)]
print("1:", outputs1)       # jonrsharpe

outputs2 = [(lambda x, y, z: [int(x), y.strip(), z.strip()])(*s.split(",")) for s in inputs]
print("2:", outputs2)       # yper

outputs3 = [z for z in map(lambda x: [int(x[0]), x[1].strip(), x[2].strip()],[s.split(",") for s in inputs])]
print("3:", outputs3)       # user2314737

outputs4 = [[int(x), y.strip(), z.strip()] for s in inputs for (x, y, z) in [s.split(",")]]
print("4:", outputs4)       # kindall

list = [(1, 2), (3, 4), (4, 6)]
[(a,b) for a,b in list]
# [a  b for a, b in enumerate(list)]        #Not working

a = [1,2,3,4,5]
b = [6,7,8,9,10]
[i+j for i,j in zip(a,b)]
c = map(sum, zip(a, b))
print(*c)
