#!/usr/bin/bash
echo "Timing: list_with_comprehension"
python -m timeit "import timeit_demo; timeit_demo.list_with_comprehension();"


echo "------------------------------------------------------------"
echo "Timing: list_without_comprehension"
python -m timeit "import timeit_demo; timeit_demo.list_without_comprehension();"


echo "------------------------------------------------------------"
echo "Timing: reverse_sort_list_with_comprehension"
python -m timeit "import timeit_demo; timeit_demo.reverse_sort_list_with_comprehension();"


echo "------------------------------------------------------------"
echo "Timing: reverse_sort_list_without_comprehension"
python -m timeit "import timeit_demo; timeit_demo.reverse_sort_list_without_comprehension();"


echo "------------------------------------------------------------"
echo "Timing: sort_list_with_comprehension"
python -m timeit "import timeit_demo; timeit_demo.sort_list_with_comprehension();"


echo "------------------------------------------------------------"
echo "Timing: sort_list_without_comprehension"
python -m timeit "import timeit_demo; timeit_demo.sort_list_without_comprehension();"


echo "------------------------------------------------------------"
echo "Timing: sorted_list_with_comprehension"
python -m timeit "import timeit_demo; timeit_demo.sorted_list_with_comprehension();"


echo "------------------------------------------------------------"
echo "Timing: sorted_list_without_comprehension"
python -m timeit "import timeit_demo; timeit_demo.sorted_list_without_comprehension();"


echo "------------------------------------------------------------"
