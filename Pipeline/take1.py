### Source: https://medium.com/asap-report/ml-pipelines-python-lifehacks-7f00356e0977

# This code you have to execute in the main file - i.e. one,
# which you execute from command line
import logging

logger = logging.getLogger("pipeline")
logger.setLevel(logging.INFO)

fh = logging.FileHandler("output.log")
ch = logging.StreamHandler()

logger.addHandler(fh)
logger.addHandler(ch)

logger.info("App start - Hello world!")


# Loggers have hierachial structure, so when you get "pipeline.XYZ",
# you inherit all settings from "pipeline" logger.
logger = logging.getLogger("pipeline.FeatureSelection")

class FeatureSelection:
  def __init__(self):
    logger.info("Creating FeatureSelection")


if __name__ == "__main__":
    # Run your pipeline

    print('Hello World!')
