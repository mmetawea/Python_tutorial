""" Plot Tutorial"""
# convert the two lists into a list of pairwise tuples
zip_generator = zip([1,2,3,4,5], [6,7,8,9,10])

# print(list(zip_generator))
# the above prints:
# [(1, 6), (2, 7), (3, 8), (4, 9), (5, 10)]

zip_generator = zip([1,2,3,4,5], [6,7,8,9,10])
# The single star * unpacks a collection into positional arguments
# print(*zip_generator)
# the above prints:
# (1, 6) (2, 7) (3, 8) (4, 9) (5, 10)

zip_generator = zip([1,2,3,4,5], [6,7,8,9,10])
x,y = zip(*zip_generator)
x,y
y
# print(x,y)

import matplotlib.pyplot as plt
plt.figure()
plt.scatter(x[:2],y[:2], s=100, c='red', label = 'Tall Students')
plt.scatter(x[2:],y[2:], s=100, c='blue', label = 'Short Students')

# add a label to the x axis
plt.xlabel('The number of times the child kicked a ball')
# add a label to the y axis
plt.ylabel('The grade of the student')
# add a title
plt.title('Relationship between ball kicking and grades')

# add a legend (uses the labels from plt.scatter)
plt.legend()
# add the legend to loc=4 (the lower right hand corner), also gets rid of the frame and adds a title
plt.legend(loc=4, frameon=True, title='Legend')

# get children from current axes (the legend is the second to last item in this list)
# plt.gca().get_children()
#
# # get the legend from the current axes
# legend = plt.gca().get_children()[-2]
#
#
# # import the artist class from matplotlib
# from matplotlib.artist import Artist
#
# def rec_gc(art, depth=0):
#     if isinstance(art, Artist):
#         # increase the depth for pretty printing
#         print("  " * depth + str(art))
#         for child in art.get_children():
#             rec_gc(child, depth+2)
#
# # Call this function on the legend artist to see what the legend is made up of
# rec_gc(plt.legend())
