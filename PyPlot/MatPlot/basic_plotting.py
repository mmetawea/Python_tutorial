import seaborn as sns
import matplotlib.pyplot as plt

# data = sns.load_dataset('brain_networks')
data = sns.load_dataset('tips')
data.shape
data.head()

fig = plt.figure()
fig.patch.set_facecolor('lightgrey')
graph1 = fig.add_subplot(3,1,1)     #(nrows, ncols, index)
graph1.plot(data.total_bill.head(10), data.tip.head(10))
graph1.set_title('Plotting')

graph2 = fig.add_subplot(3,1,2)
graph2.bar(data.total_bill[:10], data.tip[:10])
graph2.set_title('Bar Plotting')

x1 = len(data[data.tip >= 1])
x2 = len( data[(data.tip <= 2) & (data.tip > 1)] )
x3 = len( data[ (data.tip > 2)] )
graph3 = fig.add_subplot(3,1,3, facecolor = 'lightblue')
# graph3.axis('equal')
graph3.pie([x1,x2,x3], colors = ['Yellow', 'Orange', 'Red'], labels=['1$ Tip','2$ Tip', 'more than 2$ Tip'], shadow=True, radius=3)
graph3.legend(title='Descripition')
plt.show


import pandas as pd
import numpy as np
import time
from math import pi

x = np.linspace(-2*pi, 2*pi, 180)
x.shape
fx = np.sin(x)
plt.plot(x,fx)

x = np.linspace(-2*pi, 2*pi, 90)
x.shape
fx = np.sin(x)
plt.scatter(x,fx)
