#https://stackoverflow.com/questions/48541444/pandas-filtering-for-multiple-substrings-in-series
import numpy as np
def orig(col, lst):
    mask = np.logical_or.reduce([col.str.contains(i, regex=False, case=False)
                                 for i in lst])
    return mask

orig(x.Message, bl)
%timeit orig(x.Message, bl)

def using_regex(col, lst):
    """https://stackoverflow.com/a/48590850/190597 (Alex Riley)"""
    esc_lst = [re.escape(s) for s in lst]
    pattern = '|'.join(esc_lst)
    mask = col.str.contains(pattern, case=False)
    return mask

import re
using_regex(x,bl)

import ahocorasick
def using_ahocorasick(col, lst):
    A = ahocorasick.Automaton(ahocorasick.STORE_INTS)
    for word in lst:
        A.add_word(word.lower())
    A.make_automaton()
    # col = col.str.lower()
    mask = col.apply(lambda x: bool(list(A.iter(x))))
    return mask

using_ahocorasick(x,bl)
using_ahocorasick(x.Message,bl)
%timeit using_ahocorasick(x.Message,bl)
