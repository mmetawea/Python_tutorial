class Car:
    def __init__(self, w):
        self.wheels = w

    def getWheel(self):
        return self.wheels

c1 = Car(4)
c2 = c1
c1 = None
c2 = Car(4)
# Now c1 is dead object with ZERO Referencesself.
# c1 is removed
n = c2.getWheel()
print(n)
