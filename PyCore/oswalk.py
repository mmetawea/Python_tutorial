import os
import nbconvert

""" os.walk returns tuple of (root path, dir names, file names) in the folder, so you can iterate through filenames and open each file by using os.path.join(root, filename) which basically joins the root path with the file name so you can open the file."""

path = 'c:\\test'
for root, _, files in os.walk(path):
    for filename in files:
        os.chdir(root)
        ### ipynb convert
        # if os.path.splitext(filename)[1] == '.ipynb':
        #     nbconvert.HTMLExporter(filename )

        ### ipynb cleanup
        # if os.path.splitext(filename)[1] == '.ipynb': os.remove(filename )
        # [os.remove(filename) for file in os.listdir(path) if os.path.splitext(file)[1] == '.ipynb']

nbconvert.HTMLExporter('c:\\test\\04-Lists.ipynb' )
nbconvert('c:\\test\\04-Lists.ipynb')
nbconvert.PythonExporter(config=None, 'c:\\test\\04-Lists.ipynb')
nbconvert.HTMLExporter(config=None)

#Export all the notebooks in the current directory to the sphinx_howto format.
os.chdir(path)
c = None

c.NbConvertApp.notebooks = ['*.ipynb']
c.NbConvertApp.export_format = 'html'
