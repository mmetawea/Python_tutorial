### https://doughellmann.com/blog/2009/06/19/python-exception-handling-techniques/
#!/usr/bin/env python

import sys

def throws():
    raise RuntimeError('this is the error message')

def main():
    try:
        throws()
        return 0
    except Exception, error:
        sys.stderr.write('ERROR: %sn' % str(error))
        return 1
    # except Exception, error:
if __name__ == '__main__':
    sys.exit(main())
