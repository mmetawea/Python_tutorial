# https://www.asmeurer.com/blog/posts/what-happens-when-you-mess-with-hashing-in-python/
"""Hashing in Python

Python has a built in function that performs a hash called hash(). For many objects, the hash is not very surprising. Note, the hashes you see below may not be the same ones you see if you run the examples, because Python hashing depends on the architecture of the machine you are running on, and, in newer versions of Python, hashes are randomized for security purposes."""

hash(10)
# 10
hash(()) # An empty tuple
# 3527539
hash('a')
# 7851398375466560848
hash('b')
# 1872482747019534385

hash([])
# TypeError: unhashable type: 'list'
hash({})
# TypeError: unhashable type: 'dict'
hash(set())
# TypeError: unhashable type: 'set'

"""This is because Python has an additional restriction on hashing:
-     In order for an object to be hashable, it must be immutable.

This is important basically because we want the hash of an object to remain the same across the object's lifetime. But if we have a mutable object, then that object itself can change over its lifetime. But then according to our first bullet point above, that object's hash has to change too."""

# While sets are mutable and non-hashable, frozensets are..
f = frozenset([0, (), '2'])
hash(f)
# 2589119231998470230

a=()
for i in range(100):
    a = (a,)
