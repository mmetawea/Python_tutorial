def no_side_effects(cities):
    print(cities)
    cities = cities + ["Birmingham", "Bradford"]
    print(cities)

locations = ["London", "Leeds", "Glasgow", "Sheffield"]

no_side_effects(locations)
#As a new list is assigned to the parameter list in func1(), a new memory location is created for list and list becomes a local variable.
print(locations)


def side_effects(cities):
    print(cities)
    cities += ["Birmingham", "Bradford"]
    print(cities)

#We can see that Birmingham and Bradford are included in the global list locations as well, because += acts as an in-place operation.
side_effects(locations)
print(locations)

# Fixing this by passing a copy
side_effects(locations[:])
print(locations)
