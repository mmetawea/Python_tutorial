""" This tutorial video covers how to open big data files in Python using buffering.
The idea here is to efficiently open files, or even to open files that are too large to be read into memory."""

import re

# ifname='.\input\input_small.csv'
# ofname = '.\input\output.csv'

ifname = 'C:\\Users\\Mohammed\\gitlab\\insights\input\input_original.csv'
ofname = 'C:\\Users\\Mohammed\\gitlab\\insights\input\output.csv'
# pwd ; cd C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\

output = open(ofname, 'w')
with open(ifname, buffering = 200000000) as f:
    for line in f:
        # saveLine = line.replace(r"[0-9]{4,}", 'XX')
        saveLine = saveLine.lower()
        # saveLine = saveLine.strip()
        # print(saveLine)
        output.write(saveLine)

output.close()
