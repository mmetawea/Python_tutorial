import pandas as pd
import numpy as np # linear algebra

# chunksize = 10 ** 6

filename='.\input\input.csv'
# for chunk in pd.read_csv(filename, chunksize=chunksize):
#     process(chunk)

def LoadTable(filename):
    # table = pd.read_table(filename, sep=',', usecols=["Time", "Source", "Message"],
    #                     iterator=True, engine= 'c', names =["Time", "Source", "Message"], parse_dates = True)#,
    table = pd.read_table(filename, sep=',', usecols=["Message"],
                        iterator=True, engine= 'c', parse_dates = True, chunksize=200)
    return table

table = LoadTable(filename)
# print(list(table))
# print(table)
# next(table)
print(table.chunksize)
# print(table.get_chunk)
print(list(table))
# head()

# chunks=pd.read_table('aphro.csv',chunksize=1000000,sep=';',\
#        names=['lat','long','rf','date','slno'],index_col='slno',\
#        header=None,parse_dates=['date'])

# df=pd.DataFrame()
# df=pd.concat(chunk.groupby(['lat','long',chunk['date'].map(lambda x: x.year)])['rf'].agg(['sum']) for chunk in chunks)
