import os
import pandas as pd
import timeit
import readloop_chunk as loopit
import readiter_chunk as iterit
# os.chdir(os.path.dirname(sys.argv[0]))
# os.chdir('C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\input')
# os.chdir('C:\\Users\\Mohammed\\github\\fuzzy')
# %pwd
# fname='.\input\input_small.csv'
fname = '/mnt/store/home/GitLibrary/Insights/input/input_small.csv'

def normfn():
    #msgdata.Time=pd.to_datetime(msgdata.Time).dt.strftime('%Y%m%d_%H:%M')
    # PIN Digits Cleanup
    msgdata.Message=msgdata.Message.str.replace("[0-9]{3,}", 'XX')
    #msgdata.Message=re.sub(r"[0-9]{4,}", "XX", msgdata.Message)
    #msgdata.str.replace("[0-9]{4,}",RegEX=True)

    #Sort DataFrame
    #msgdata.sort_values(by['source', 'message','time'], inplace=True)
    #msgdata.groupby(msgdata.Message)

## Timing - How to Call
# t=timeit.Timer(stmt=LoadTable, setup="pass")
# print('\nelapsed time: {0:.3f} when using open fun.'.format(t.timeit(number=10)))
t1= timeit.Timer(stmt= loopit, setup="pass")
t1= timeit.Timer(stmt= readiter_chunk, setup="pass")

print('\nelapsed time: {0:.3f} when using Read Loop in Chunks.'.format(t1.timeit(number=10)))
print('\nelapsed time: {0:.3f} when using Read Iters in Chunks.'.format(t2.timeit(number=10)))

dir(readiter_chunk)

print(readiter_chunk.main())


""" Results
C:\Users\Mohammed\github\fuzzy>py readloop_chunk.py
Time of execution is:  0.1837039999999998  msecs

C:\Users\Mohammed\github\fuzzy>py readloop_chunk.py
Time of execution is:  0.1544690000000043  msecs

C:\Users\Mohammed\github\fuzzy>py readiter_chunk.py
Time of execution is:  0.14854300000000098  msecs

C:\Users\Mohammed\github\fuzzy>py readiter_chunk.py
Time of execution is:  8.65539000000004  msecs

C:\Users\Mohammed\github\fuzzy>py readloop_chunk.py
Traceback (most recent call last):
  File "readloop_chunk.py", line 24, in <module>
    for piece in read_in_chunks(fname):
NameError: name 'fname' is not defined

C:\Users\Mohammed\github\fuzzy>py readloop_chunk.py
Traceback (most recent call last):
  File "readloop_chunk.py", line 24, in <module>
    for piece in read_in_chunks(fname):
NameError: name 'fname' is not defined

C:\Users\Mohammed\github\fuzzy>py readloop_chunk.py
Time of execution is:  4.758904000000001  msecs

C:\Users\Mohammed\github\fuzzy>py readiter_chunk.py
Time of execution is:  4.4894729999999665  msecs

C:\Users\Mohammed\github\fuzzy>py readiter_chunk.py
Time of execution is:  5.575890000000028  msecs

C:\Users\Mohammed\github\fuzzy>py readloop_chunk.py
Time of execution is:  8.421120000000004  msecs

C:\Users\Mohammed\github\fuzzy>py readloop_chunk.py
Time of execution is:  5.127891000000003  msecs

C:\Users\Mohammed\github\fuzzy>py readiter_chunk.py
Time of execution is:  4.5068560000000035  msecs

C:\Users\Mohammed\github\fuzzy>py readiter_chunk.py
Time of execution is:  4.321966999999982  msecs

C:\Users\Mohammed\github\fuzzy>py readiter_chunk.py
Time of execution is:  127.4403409999999  msecs

C:\Users\Mohammed\github\fuzzy>py readloop_chunk.py
Time of execution is:  121.639266  msecs


C:\Users\Mohammed\github\fuzzy>py readloop_chunk.py
Time of execution is:  487.80071599999997  msecs

C:\Users\Mohammed\github\fuzzy>py readiter_chunk.py
Time of execution is:  303.24720399999995  msecs
"""
