""" Instead of playing with offsets in the file, try to build and yield lists of 10000 elements from a loop: """

# ifname='.\input\input_small.csv'
# ofname = '.\input\output.csv'

ifname = 'C:\\Users\\Mohammed\\gitlab\\insights\input\input_small.csv'
ofname = 'C:\\Users\\Mohammed\\gitlab\\insights\input\output.csv'
# pwd ; cd C:\\Users\\Mohammed\\Google Drive\\workplace\\insights\\


def read_large_file(file_handler, block_size=10):
    block = []
    for line in file_handler:
        block.append(line)
        if len(block) == block_size:
            yield block
            block = []

    # don't forget to yield the last block
    if block:
        yield block

with open(ifname) as file_handler:
    for block in read_large_file(file_handler):
        print(block)
        print("----- Sep -----")

# open(ifname)
